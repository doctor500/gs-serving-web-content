pipeline {
   agent any

   stages {
      stage('Clean working directory'){
          steps {
              deleteDir()
          }
      }
      stage('Pull Git Repo') {
         steps {
             git 'https://gitlab.com/doctor500/gs-serving-web-content.git'
         }
      }
      stage('Build JAR') {
         steps {
            dir('complete/') {
                sh "./gradlew bootJar"
            }
         }
      }
      stage('Build docker image'){
          steps{
              dir('complete/') {
                    sh  "\$(aws ecr get-login --no-include-email) && \
                        docker build -t 867097165376.dkr.ecr.us-east-2.amazonaws.com/springjenkins:build-$BUILD_NUMBER . && \
                        docker push 867097165376.dkr.ecr.us-east-2.amazonaws.com/springjenkins:build-$BUILD_NUMBER"
                }
          }
      }
      stage('Deploy to AWS Docker VM') {
         steps {
            sshPublisher(
                publishers: [
                    sshPublisherDesc(
                        configName: 'Amazon Linux AMI', 
                        transfers: [
                            sshTransfer(
                                cleanRemote: false, 
                                excludes: '', 
                                execCommand:"\$(aws ecr get-login --no-include-email) && \
                                            docker stop spring_jenkins || true && \
                                            docker rm spring_jenkins || true",
                                execTimeout: 120000, 
                                flatten: false, 
                                makeEmptyDirs: false, 
                                noDefaultExcludes: false, 
                                patternSeparator: '[, ]+', 
                                remoteDirectory: '', 
                                remoteDirectorySDF: false, 
                                removePrefix: '', 
                                sourceFiles: ''),
                            sshTransfer(
                                cleanRemote: false, 
                                excludes: '', 
                                execCommand:"docker run -d -p 8080:8080 --name spring_jenkins \
                                            867097165376.dkr.ecr.us-east-2.amazonaws.com/springjenkins:build-$BUILD_NUMBER",
                                execTimeout: 120000, 
                                flatten: false, 
                                makeEmptyDirs: false, 
                                noDefaultExcludes: false, 
                                patternSeparator: '[, ]+', 
                                remoteDirectory: '', 
                                remoteDirectorySDF: false, 
                                removePrefix: '', 
                                sourceFiles: '')], 
                            usePromotionTimestamp: false, 
                            useWorkspaceInPromotion: false, 
                            verbose: false)])
         }
      }
   }
}
